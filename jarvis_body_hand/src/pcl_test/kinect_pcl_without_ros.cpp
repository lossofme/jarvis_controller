#include <pcl/io/openni_grabber.h>
#include <pcl/visualization/cloud_viewer.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>

// #include <ros/ros.h>

#include <pcl/point_cloud.h>  
// #include <pcl_conversions/pcl_conversions.h>  
// #include <sensor_msgs/PointCloud2.h> 

 class OpenNiViewer
 {
   public:
     OpenNiViewer (/*ros::NodeHandle& nodeHandle*/)
     /*:nodeHandle_(nodeHandle),*/ 
      :viewer ("PCL OpenNI Viewer") 
     {

      // pcl_pub = nodeHandle_.advertise<sensor_msgs::PointCloud2> ("/point2", 10,true);

     }

     void cloud_cb_ (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr &cloud)
     {





        pcl::VoxelGrid<pcl::PointXYZRGBA> vg;
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGBA>), 
                                                cloud_f (new pcl::PointCloud<pcl::PointXYZRGBA>);


        //LIMIT PCL Z AXIS FROM 0 TO 1.2
        pcl::PassThrough<pcl::PointXYZRGBA> pass;
        pass.setInputCloud (cloud);
        pass.setFilterFieldName ("z");
        pass.setFilterLimits (0.0, 20);
        //pass.setFilterLimitsNegative (true);
        pass.filter (*cloud_filtered);




        // Create the filtering object: downsample the dataset using a leaf size of 1cm
        vg.setInputCloud (cloud_filtered);
        vg.setLeafSize (0.05f, 0.05f, 0.05f);
        vg.filter (*cloud_filtered);
        std::cout << "PointCloud after filtering has: " << cloud_filtered->points.size ()  << " data points." << std::endl; //*




/*
        pcl::toROSMsg(*cloud_filtered, output);  
        output.header.frame_id = "camera_link";
        output.header.stamp = ros::Time::now();
        pcl_pub.publish(output); */

       if (!viewer.wasStopped())
         viewer.showCloud (cloud_filtered);
     }

     void run()
     {
       pcl::Grabber* interface = new pcl::OpenNIGrabber();

       boost::function<void (const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr&)> f =
         boost::bind (&OpenNiViewer::cloud_cb_, this, _1);

       interface->registerCallback (f);

       interface->start ();

       while (!viewer.wasStopped())
       {
         boost::this_thread::sleep (boost::posix_time::seconds (1));
       }

       interface->stop ();
     }


     // ros::NodeHandle& nodeHandle_;
     pcl::visualization::CloudViewer viewer;

     // ros::Publisher pcl_pub;
     // sensor_msgs::PointCloud2 output;  
 };

 int main(int argc, char** argv)
 {

   // ros::init(argc, argv, "kinect_processed_node");
   // ros::NodeHandle nodeHandle("~");
   OpenNiViewer v;
   v.run();
   return 0;
 }