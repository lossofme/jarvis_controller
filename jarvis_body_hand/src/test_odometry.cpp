#include <ros/ros.h>

#include <sensor_msgs/Joy.h>
#include <std_msgs/Float64.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <Eigen/Dense>
#include <eigen_conversions/eigen_msg.h>
#include <tf_conversions/tf_eigen.h>


#include <control_msgs/FollowJointTrajectoryFeedback.h>

#define Kv 6.0
#define Kth 1.0


/*
 * Description : Wheel based navigation and control system
 * Author      : Thanabadee Bulunseechart
 */


template<typename T>
void pop_front(std::vector<T>& vec)
{
    assert(!vec.empty());
    vec.erase(vec.begin());
}
enum
{
	x=0,
	y,
	z
};
enum
{
	RC_MODE =0,
	WP_MODE,
	NUM_MODE
};
namespace DriveBase 
{
using namespace Eigen;

class Wheel
{
	
	public:
		Wheel()
		{

		}
		void joyCb(const sensor_msgs::Joy::ConstPtr& msg)
		{

			if(msg->axes[0]!=0){
				Vth = 3*msg->axes[0];
			}else{
				//This will effect only non-manual mode
				Vth = 0;
			}

			Vd[x] =  msg->axes[4];
			Vd[y] =  msg->axes[3];
			Vd[z] = 0;
			Vd_norm = 3*Vd.norm();
			
			dth = atan2(Vd[y], Vd[x]);
		}

		void AllMotorCallback(const control_msgs::FollowJointTrajectoryFeedback::ConstPtr &msg) 
		{
			double temp_data[4];
			int Count_mot = 0;
			for(int joint_idx = 0; joint_idx<4; joint_idx++)
				for(int i =0; i< msg->joint_names.size(); i++)
				{
					if(msg->joint_names[i]=="wheel"+std::to_string(joint_idx))
					{
						temp_data[joint_idx] = msg->actual.positions[joint_idx];
						Count_mot++;
					}

				}

			if(Count_mot==4)
				memcpy(AngularVelWheel, &temp_data, sizeof(temp_data));
			else
				ROS_INFO("CAN'T RECEIVE ALL OR SOME MOT");
		}

	private:
		Vector3d Vd;
		double Vd_norm;
		double Vth;
		double dth;

		double AngularVelWheel[4];

};
};
using namespace DriveBase;
using namespace Eigen;
int main(int argc, char** argv)
{
	ros::init(argc, argv, "jarvis_drive_test");
	ros::NodeHandle nodeHandle_("~");
	//INITIALIZE VARIABLE
	Wheel Drive;
	ros::Subscriber subJoy_ = nodeHandle_.subscribe("/joy", 1,
                                      &Wheel::joyCb, &Drive);

	// subMot0 = nodeHandle_.subscribe("/wheel0_controller/state", 10, &Wheel::Mot0Cb, &Drive);
	// subMot1 = nodeHandle_.subscribe("/wheel1_controller/state", 10, &Wheel::Mot1Cb, &Drive);
	// subMot2 = nodeHandle_.subscribe("/wheel2_controller/state", 10, &Wheel::Mot2Cb, &Drive);
	// subMot3 = nodeHandle_.subscribe("/wheel3_controller/state", 10, &Wheel::Mot3Cb, &Drive);

	ros::Subscriber subMotAll_ = nodeHandle_.subscribe("/f_arm_controller/state", 1,
                                      &Wheel::AllMotorCallback, &Drive);


	ROS_INFO("Odometry test: Successful launch");

	ros::Rate r(10);

	while(ros::ok())
	{


		r.sleep();
		ros::spinOnce();
	}
	
	return 0;
}





