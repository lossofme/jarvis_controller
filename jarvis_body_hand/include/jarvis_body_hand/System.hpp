#pragma once

#include "jarvis_body_hand/Algorithm.hpp"
#include "jarvis_body_hand/record_hand_position.hpp"
#include "jarvis_body_hand/kinematic.hpp"
#include "jarvis_body_hand/LaserDrive.hpp"
#include "jarvis_body_hand/actuator.hpp"
#include "jarvis_body_hand/control_input.hpp"
#include "jarvis_body_hand/drivebase.hpp"
#include "jarvis_body_hand/voice.hpp"
 
// ROS
#include <ros/ros.h>
#include <sensor_msgs/Temperature.h>
#include <std_srvs/Trigger.h>
#include <dynamixel_msgs/JointState.h>
#include <dynamixel_msgs/MotorStateList.h>
#include <control_msgs/FollowJointTrajectoryFeedback.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <sensor_msgs/JointState.h>

#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>


#include<thread>


namespace jarvis_body_hand {
using namespace Eigen;
class Record_hand;
/*!
 * Main class for the node to handle the ROS interfacing.
 */
class System
{
 public:
  /*!
   * Constructor.
   * @param nodeHandle the ROS node handle.
   */
  System(ros::NodeHandle& nodeHandle);

  /*!
   * Destructor.
   */
  virtual ~System();

 private:
  //Parameters
  std::vector<int> JOINT_IK_DIR;
  std::vector<int> JOINT_RVIZ;
  std::vector<int> JOINT_START_IDX;
  std::vector<std::string> JOINT_GROUP;
  std::string OBJECT;

  int JOINT_N;

  /*!
   * Reads and verifies the ROS parameters.
   * @return true if successful.
   */
  bool readParameters();

  /*!
   * Combination sequence of Task Grip and Place
   * Need determined offset and performance 
   */
  bool PrepareGrip();
  bool GripAt(double x, double y, double z);
  bool PlaceAt(double x, double y, double z);
  bool GripAt(Vector3d v);
  bool PlaceAt(Vector3d v);
  /*!
   * ROS topic callback method.
   * @param message the received message.
   */
  void RvizCallback(const sensor_msgs::JointState::ConstPtr &message);
  void Desired_xyzCallback(const geometry_msgs::Vector3::ConstPtr &msg);
  // void trajectoryCallback(const dynamixel_msgs::MotorStateList::ConstPtr &message);

  /*!
   * ROS service server callback.
   * @param request the request of the service.
   * @param response the provided response.
   * @return true if successful, false otherwise.
   */
  bool serviceCallback(std_srvs::Trigger::Request& request,
                       std_srvs::Trigger::Response& response);

  //! ROS node handle.
  ros::NodeHandle& nodeHandle_;

  //! ROS topic subscriber.
  ros::Subscriber sub_Rviz_;
  ros::Subscriber sub_Desired_xyz_;

  //! Temporary publisher (TODO(MAX) : MORE WORK TO DO HERE)
  // ros::Publisher pubGrip0;


  //! ROS service server.
  ros::ServiceServer serviceServer_;


  //! record and playback computation object.
  Record_hand* rec_hand_;
  std::thread* t_rec_hand;

  //! kinematic 
  Kinematic* kinetic_;

  //! record and playback computation object.
  Actuator* actuator_;
  std::thread* t_actuator;

  //! control input object
  ControlInput* controlin_;

  //! record and playback computation object.
  DriveBase* drivebase_;
  std::thread* t_drivebase;

  //! RObot talker
  Voice* voice_;



  const double RIGHT_HAND_Y = -0.153;
  const double TABLE_HEIGHT = 0.55;//0.65;
  const double RADIUS = 0.3;//0.43;
};

} /* namespace */
