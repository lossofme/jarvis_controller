#pragma once


#include <ros/ros.h>
#include <ros/package.h>

#include <dynamixel_msgs/MotorStateList.h>
#include <dynamixel_msgs/MotorState.h>
#include <control_msgs/FollowJointTrajectoryFeedback.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <dynamixel_msgs/JointState.h>
#include <std_msgs/Float64.h>

namespace jarvis_body_hand {

/*!
 * Class containing the algorithmic part of the package.
 */
class Actuator
{

 private:
  //! Internal variable to hold the current average.
  //! ROS node handle.
  ros::NodeHandle& nodeHandle_;
  //! ROS topic publisher.
  std::vector<ros::Publisher> pubMot;


  bool STOP_THREAD;
  bool PAUSED;
  bool REQUEST_RESET;

  std::vector<dynamixel_msgs::MotorStateList::ConstPtr> MotorListMsgQueue;

  std::vector<double> JOINT_SCALE;
  std::vector<double> JOINT_RAW_HOME;
  std::vector<int> JOINT_START_IDX;
  std::vector<std::string> JOINT_GROUP;
  int JOINT_N;

  void readParameters();
 public:
  
  bool updated;


  /*!
   * Constructor.
   */
  Actuator(ros::NodeHandle& nodeHandle);

  void Run();
  bool Stop();
  void RequestReset();
  bool FINISH() const;
  /*!
   * Destructor.
   */
  virtual ~Actuator();





  
};

} /* namespace */
