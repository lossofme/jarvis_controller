#include <ros/ros.h>
#include "jarvis_perception/System.hpp"



#include <sensor_msgs/Image.h>
#include <std_msgs/String.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <geometry_msgs/Vector3.h>
//---pcl---
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
// PCL specific includes
//#include <pcl/ros/conversions.h>
#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>

#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>
//---------
using namespace std;
using namespace cv;



class ImageGrabber
{
public:
    ImageGrabber(jarvis_perception::System* pSys_):pSys(pSys_){}

    void GrabImage(const sensor_msgs::ImageConstPtr& msg);
	void GrabPointcloud(const sensor_msgs::PointCloud2& cloud);

    jarvis_perception::System* pSys;

	static void mouseCallback(int event, int x, int y, int flags, void *param);
    void doMouseCallback(int event, int x, int y, int flags);
};









int main(int argc, char** argv)
{
	ros::init(argc, argv, "jarvis_perception");
	ros::NodeHandle nodeHandle("~");

	jarvis_perception::System System(nodeHandle);

	ImageGrabber igb(&System);


	ros::Subscriber sub = nodeHandle.subscribe("/camera/rgb/image_color", 1,  &ImageGrabber::GrabImage, &igb);
	ros::Subscriber subDepth = nodeHandle.subscribe("/camera/depth_registered/points", 1, &ImageGrabber::GrabPointcloud, &igb);



	printf("start click_objects\n");
	cvNamedWindow("MAIN", 1 );
	cvSetMouseCallback("MAIN", ImageGrabber::mouseCallback, &igb);





	ros::spin();
	return 0;
}







void ImageGrabber::GrabPointcloud(const sensor_msgs::PointCloud2& cloud) {
	static int count = 0;
	if(++count % 5 == 0) 
	{
		count = 0;
		pSys->SetPointcloud(cloud);
	}
}



void ImageGrabber::GrabImage(const sensor_msgs::ImageConstPtr& msg)
{
	static int count = 0;
	if(++count % 5 == 0) 
	{
		count = 0;



	    pSys->SetImg(msg);

	    int inKey = 0;

	    
	     // Copy the ros image message to cv::Mat.
	    cv_bridge::CvImageConstPtr cv_ptr;
	    try
	    {
	        cv_ptr = cv_bridge::toCvShare(msg);
	    }
	    catch (cv_bridge::Exception& e)
	    {
	        ROS_ERROR("cv_bridge exception: %s", e.what());
	        return;
	    }

	 //    IplImage *inFrame  = NULL;
	 //    inFrame = cvCreateImage(cvSize(640, 480), 8, 3);
		// for(int i=0;i<640*480;i++)
		// {
		// 		inFrame->imageData[i*3] = msg->data[i*3];
		// 		inFrame->imageData[i*3+1] = msg->data[i*3+1];
		// 		inFrame->imageData[i*3+2] = msg->data[i*3+2];
		// }
		circle(cv_ptr->image, cvPoint(640/2,480/2), 20, CV_RGB(255,0,0), 3, CV_AA, 0);
		imshow("MAIN",cv_ptr->image);

		inKey = cvWaitKey(1);
		if(inKey == 27){
			exit(0);
		}
	}
}


void ImageGrabber::mouseCallback(int event, int x, int y, int flags, void *param)
{
	//Magic trick to put Mouse event into class!!
	ImageGrabber *self = static_cast<ImageGrabber*>(param);
    self->doMouseCallback(event, x, y, flags);
}

void ImageGrabber::doMouseCallback(int event, int x, int y, int flags)
{
	if(event == CV_EVENT_LBUTTONUP) 
	{
		ROS_INFO("click_at : x:%d y:%d",x,y);
		pSys->SelectPointcloud(x,y);

	}
	if(event == CV_EVENT_RBUTTONUP) 
	{
        ROS_INFO("R_click");
        // pSys->SelectPointcloud(x,y);

	}
}












